from sklearn.model_selection import train_test_split
import numpy as np
import csv
import tensorflow as tf
import time
from sklearn.metrics import confusion_matrix, classification_report
import sys


EXAMPLES_PATH = './trainset.csv'
CLASSES_PATH = './trainset_klasy.csv'
NUMBER_OF_CLASSES = 3
EXAMPLE_LENGTH = 15*125
LEARNING_RATE = 0.01
BATCH_SIZE = 64
MAX_EPOCHS = 100
MODEL_FILE_PATH = "./model"

classes2number = {'N': 0, 'A': 1, 'O': 2}

TRAIN_FROM_SCRATCH = True
LOWEST_VALID_ERROR_FROM_PREV_TRAINING = 0.744692226703


def train():
    examples, classes = read_examples(EXAMPLES_PATH, CLASSES_PATH)
    examples = np.asarray(examples, dtype=np.float32)
    examples = np.reshape(examples, (len(examples), 1, len(examples[0]), 1))
    classes = np.asarray(classes, dtype=np.float32)
    classes = to_onehot(classes, NUMBER_OF_CLASSES)

    train_set, test_set, train_classes, test_classes = train_test_split(examples, classes, test_size=0.3)
    valid_set, test_set, valid_classes, test_classes = train_test_split(test_set, test_classes, test_size=0.5)

    x = tf.placeholder(tf.float32, shape=[None, 1, EXAMPLE_LENGTH, 1])
    dropout = tf.placeholder(tf.float32)
    y_ = tf.placeholder(tf.float32, shape=[None, NUMBER_OF_CLASSES])

    y = net(x, dropout)

    cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))
    train_step = tf.train.AdagradOptimizer(LEARNING_RATE).minimize(cross_entropy)
    error = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))
    prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(prediction, tf.float32))

    saver = tf.train.Saver()

    lowest_valid_error = 9999
    best_epoch = -1
    train_batches_count = np.int(np.ceil(len(train_set) / np.float(BATCH_SIZE)))
    valid_batches_count = np.int(np.ceil(len(valid_set) / np.float(BATCH_SIZE)))
    test_batches_count = np.int(np.ceil(len(test_set) / np.float(BATCH_SIZE)))

    start = time.time()
    with tf.Session() as session:
        if not TRAIN_FROM_SCRATCH:
            print('loading model from previous training')
            lowest_valid_error = LOWEST_VALID_ERROR_FROM_PREV_TRAINING
            saver.restore(session, MODEL_FILE_PATH)
        else:
            print('training from scratch')
            session.run(tf.global_variables_initializer())
        for epoch in range(MAX_EPOCHS):
            epoch_start = time.time()
            print('-------------------------------')
            print('epoch: ', epoch + 1)

            for i in range(train_batches_count):
                train_step.run(feed_dict={
                    x: train_set[i * BATCH_SIZE:(i + 1) * BATCH_SIZE],
                    y_: train_classes[i * BATCH_SIZE:(i + 1) * BATCH_SIZE],
                    dropout: 0.5})

            train_error = 0
            for i in range(train_batches_count):
                train_error += error.eval(feed_dict={
                    x: train_set[i * BATCH_SIZE:(i + 1) * BATCH_SIZE],
                    y_: train_classes[i * BATCH_SIZE:(i + 1) * BATCH_SIZE],
                    dropout: 1.0})
            print('train error: ', train_error / train_batches_count)

            valid_error = 0
            valid_accuracy = 0
            for i in range(valid_batches_count):
                valid_error += error.eval(feed_dict={
                    x: valid_set[i * BATCH_SIZE:(i + 1) * BATCH_SIZE],
                    y_: valid_classes[i * BATCH_SIZE:(i + 1) * BATCH_SIZE],
                    dropout: 1.0})
                valid_accuracy += accuracy.eval(feed_dict={
                    x: valid_set[i * BATCH_SIZE:(i + 1) * BATCH_SIZE],
                    y_: valid_classes[i * BATCH_SIZE:(i + 1) * BATCH_SIZE],
                    dropout: 1.0})
            print('valid error: ', valid_error / valid_batches_count)
            print('valid acc:', valid_accuracy / valid_batches_count)

            if lowest_valid_error > valid_error / valid_batches_count:
                print('validation error is lower than previous, saving model...')
                saver.save(session, MODEL_FILE_PATH)
                lowest_valid_error = valid_error / valid_batches_count
                best_epoch = epoch + 1

            print('epoch took: ', time.time() - epoch_start)
        print('training took: ', time.time() - start)
    print('===========================================')

    with tf.Session() as session:
        print('loading model from epoch ', best_epoch)
        print('with validation error: ', lowest_valid_error)
        saver.restore(session, MODEL_FILE_PATH)

        test_error = 0
        test_accuracy = 0
        for i in range(test_batches_count):
            test_error += error.eval(feed_dict={
                x: test_set[i * BATCH_SIZE:(i + 1) * BATCH_SIZE],
                y_: test_classes[i * BATCH_SIZE:(i + 1) * BATCH_SIZE],
                dropout: 1.0})
            test_accuracy += accuracy.eval(feed_dict={
                x: test_set[i * BATCH_SIZE:(i + 1) * BATCH_SIZE],
                y_: test_classes[i * BATCH_SIZE:(i + 1) * BATCH_SIZE],
                dropout: 1.0})
        print('test error: ', test_error / test_batches_count)
        print('test acc:', test_accuracy / test_batches_count)

        y_true = np.argmax(test_classes, axis=1)
        y_pred = []
        for i in range(test_batches_count):
            test_batch = test_set[i * BATCH_SIZE:(i + 1) * BATCH_SIZE]
            preds = np.argmax(y.eval(feed_dict={x: test_batch, dropout: 1.0}), axis=1)
            y_pred += preds.tolist()
        y_true = y_true[0:len(y_pred)]

        conf_matrix = confusion_matrix(y_true, y_pred)
        report = classification_report(y_true, y_pred, target_names=['N', 'A', 'O'])

        print(report)
        for row in conf_matrix:
            sys.stdout.write('\n')
            for col in row:
                sys.stdout.write(str(col) + '\t')
            sys.stdout.write('\n')

    pass

    pass


def net(x, dropout):

    conv_layer = x

    conv_layer = tf.layers.conv2d(conv_layer, 32, (1, 3), padding='same')
    conv_layer = tf.layers.conv2d(conv_layer, 32, (1, 3), padding='same')
    conv_layer = tf.layers.max_pooling2d(conv_layer, (1, 2), (1, 2))
    conv_layer = tf.nn.relu(conv_layer)
    print(conv_layer.get_shape())

    conv_layer = tf.layers.conv2d(conv_layer, 64, (1, 3), padding='same')
    conv_layer = tf.layers.conv2d(conv_layer, 64, (1, 3), padding='same')
    conv_layer = tf.layers.max_pooling2d(conv_layer, (1, 2), (1, 2))
    conv_layer = tf.nn.relu(conv_layer)
    print(conv_layer.get_shape())

    conv_layer = tf.layers.conv2d(conv_layer, 96, (1, 3), padding='same')
    conv_layer = tf.layers.conv2d(conv_layer, 96, (1, 3), padding='same')
    conv_layer = tf.layers.max_pooling2d(conv_layer, (1, 2), (1, 2))
    conv_layer = tf.nn.relu(conv_layer)
    print(conv_layer.get_shape())

    conv_layer = tf.layers.conv2d(conv_layer, 128, (1, 3), padding='same')
    conv_layer = tf.layers.conv2d(conv_layer, 128, (1, 3), padding='same')
    conv_layer = tf.layers.max_pooling2d(conv_layer, (1, 2), (1, 2))
    conv_layer = tf.nn.relu(conv_layer)
    print(conv_layer.get_shape())

    conv_layer = tf.layers.conv2d(conv_layer, 32, (1, 1), padding='same')
    print(conv_layer.get_shape())

    flatten_layer = tf.contrib.layers.flatten(conv_layer)
    print(flatten_layer.get_shape())

    fc_layer = flatten_layer

    fc_layer = tf.nn.dropout(fc_layer, keep_prob=dropout)
    fc_layer = tf.layers.dense(fc_layer, 512, activation=tf.nn.relu)
    fc_layer = tf.nn.dropout(fc_layer, keep_prob=dropout)
    fc_layer = tf.layers.dense(fc_layer, 128, activation=tf.nn.relu)
    fc_layer = tf.layers.dense(fc_layer, NUMBER_OF_CLASSES, activation=tf.nn.softmax)

    y = fc_layer
    return y


def to_onehot(classes, number_of_classes):
    num_labels = len(classes)
    index_offset = np.arange(num_labels) * number_of_classes
    labels_one_hot = np.zeros((num_labels, number_of_classes), dtype=np.float32)
    labels_one_hot.flat[np.int32(index_offset + classes.ravel())] = 1.0
    return labels_one_hot


def read_examples(examples_path, classes_path):
    examples = []
    with open(examples_path, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            examples.append(row)

    classes = []
    with open(classes_path, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            classes.append(classes2number[row[0]])

    return examples, classes


train()
