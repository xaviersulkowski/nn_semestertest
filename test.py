import tensorflow as tf
import csv
from main import MODEL_FILE_PATH
from main import EXAMPLE_LENGTH
from main import classes2number
from main import net
import numpy as np


TEST_FILE_PATH = 'testset1.csv'
RESULT_FILE_PATH = 'result.csv'
BATCH_SIZE = 64

number2class = {v: k for k, v in classes2number.iteritems()}


def test():
    signals = read_signals(TEST_FILE_PATH)
    signals = np.asarray(signals, dtype=np.float32)
    signals = np.reshape(signals, (len(signals), 1, len(signals[0]), 1))
    x = tf.placeholder(tf.float32, shape=[None, 1, EXAMPLE_LENGTH, 1])
    dropout = tf.placeholder(tf.float32)
    y = net(x, dropout)
    saver = tf.train.Saver()

    batches_count = np.int(np.ceil(len(signals) / np.float(BATCH_SIZE)))
    with tf.Session() as session:
        print('loading model... ', MODEL_FILE_PATH)
        saver.restore(session, MODEL_FILE_PATH)

        preds = []
        print('evaluating...')
        for i in range(batches_count):
            test_batch = signals[i * BATCH_SIZE:(i + 1) * BATCH_SIZE]
            preds_tmp = np.argmax(y.eval(feed_dict={x: test_batch, dropout: 1.0}), axis=1)
            preds += preds_tmp.tolist()

    print('saving...')
    with open(RESULT_FILE_PATH, 'wb') as csvfile:
        writer = csv.writer(csvfile, delimiter=' ')
        for pred in preds:
            writer.writerow(number2class[pred])

    pass


def read_signals(signals_path):
    examples = []
    with open(signals_path, 'rb') as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            examples.append(row)
    return examples


test()